import React, {useState} from 'react';
import './index.css';

export default function Conversor({...props}) {

    const [valA, setValA] = useState("");
    const [valB, setValB] = useState();

    const converter = () => {
        // console.log(valA);
        let de_para = `${props.moedaA}_${props.moedaB}`.toUpperCase();
        let url = `https://free.currconv.com/api/v7/convert?q=${de_para}&compact=ultra&apiKey=947f736158552e771e4e`;

        fetch(url)
            .then( res => {
                return res.json();
            })
            .then( json => {
                let cotacao = json[de_para];
                let moedaB_valor = +(parseFloat( +valA ) * cotacao).toFixed(2);
                // var _val = moedaB_valor.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
                setValB(moedaB_valor)
            })
        ;

    }

    return (
        <div className="conversor">
            <h2>{props.moedaA} para {props.moedaB}</h2>
            <input type="text" onChange={ (event) => setValA(event.target.value)}></input>
            <input type="button" value="Converter" onClick={converter} />
            <h2>{valB}</h2>
        </div>
    )
}
